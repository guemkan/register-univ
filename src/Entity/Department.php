<?php

namespace App\Entity;


use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DepartmentRepository::class)
 */
class Department
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"r"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"r"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=DepartmentFiliere::class, mappedBy="department")
     */
    private $departmentFilieres;

    /**
     * @ORM\OneToMany(targetEntity=DepartmentChoice::class, mappedBy="departmentId")
     */
    private $departmentChoices;

    public function __construct()
    {
        $this->departmentFilieres = new ArrayCollection();
        $this->departmentChoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, DepartmentFiliere>
     */
    public function getDepartmentFilieres(): Collection
    {
        return $this->departmentFilieres;
    }

    public function addDepartmentFiliere(DepartmentFiliere $departmentFiliere): self
    {
        if (!$this->departmentFilieres->contains($departmentFiliere)) {
            $this->departmentFilieres[] = $departmentFiliere;
            $departmentFiliere->setDepartment($this);
        }

        return $this;
    }

    public function removeDepartmentFiliere(DepartmentFiliere $departmentFiliere): self
    {
        if ($this->departmentFilieres->removeElement($departmentFiliere)) {
            // set the owning side to null (unless already changed)
            if ($departmentFiliere->getDepartment() === $this) {
                $departmentFiliere->setDepartment(null);
            }
        }

        return $this;
    }
    public function __toString(){
        return $this->name; //or anything else
    }

    /**
     * @return Collection<int, DepartmentChoice>
     */
    public function getDepartmentChoices(): Collection
    {
        return $this->departmentChoices;
    }

    public function addDepartmentChoice(DepartmentChoice $departmentChoice): self
    {
        if (!$this->departmentChoices->contains($departmentChoice)) {
            $this->departmentChoices[] = $departmentChoice;
            $departmentChoice->setDepartmentId($this);
        }

        return $this;
    }

    public function removeDepartmentChoice(DepartmentChoice $departmentChoice): self
    {
        if ($this->departmentChoices->removeElement($departmentChoice)) {
            // set the owning side to null (unless already changed)
            if ($departmentChoice->getDepartmentId() === $this) {
                $departmentChoice->setDepartmentId(null);
            }
        }

        return $this;
    }
}
