<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="le nom ne doit pas etre vide")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank
     *
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $countryBirth;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $regionOrigin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $departmentOrigin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $districtOrigin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fatherName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fatherProfession;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motherName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motherProfession;

    /**
     * @ORM\OneToMany(targetEntity=StudentRegister::class, mappedBy="person")
     */
    private $studentRegisters;

    public function __construct()
    {
        $this->studentRegisters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getCountryBirth(): ?string
    {
        return $this->countryBirth;
    }

    public function setCountryBirth(string $countryBirth): self
    {
        $this->countryBirth = $countryBirth;

        return $this;
    }

    public function getRegionOrigin(): ?string
    {
        return $this->regionOrigin;
    }

    public function setRegionOrigin(string $regionOrigin): self
    {
        $this->regionOrigin = $regionOrigin;

        return $this;
    }

    public function getDepartmentOrigin(): ?string
    {
        return $this->departmentOrigin;
    }

    public function setDepartmentOrigin(string $departmentOrigin): self
    {
        $this->departmentOrigin = $departmentOrigin;

        return $this;
    }

    public function getDistrictOrigin(): ?string
    {
        return $this->districtOrigin;
    }

    public function setDistrictOrigin(string $districtOrigin): self
    {
        $this->districtOrigin = $districtOrigin;

        return $this;
    }

    public function getFatherName(): ?string
    {
        return $this->fatherName;
    }

    public function setFatherName(string $fatherName): self
    {
        $this->fatherName = $fatherName;

        return $this;
    }

    public function getFatherProfession(): ?string
    {
        return $this->fatherProfession;
    }

    public function setFatherProfession(string $fatherProfession): self
    {
        $this->fatherProfession = $fatherProfession;

        return $this;
    }

    public function getMotherName(): ?string
    {
        return $this->motherName;
    }

    public function setMotherName(string $motherName): self
    {
        $this->motherName = $motherName;

        return $this;
    }

    public function getMotherProfession(): ?string
    {
        return $this->motherProfession;
    }

    public function setMotherProfession(string $motherProfession): self
    {
        $this->motherProfession = $motherProfession;

        return $this;
    }

    /**
     * @return Collection<int, StudentRegister>
     */
    public function getStudentRegisters(): Collection
    {
        return $this->studentRegisters;
    }

    public function addStudentRegister(StudentRegister $studentRegister): self
    {
        if (!$this->studentRegisters->contains($studentRegister)) {
            $this->studentRegisters[] = $studentRegister;
            $studentRegister->setPerson($this);
        }

        return $this;
    }

    public function removeStudentRegister(StudentRegister $studentRegister): self
    {
        if ($this->studentRegisters->removeElement($studentRegister)) {
            // set the owning side to null (unless already changed)
            if ($studentRegister->getPerson() === $this) {
                $studentRegister->setPerson(null);
            }
        }

        return $this;
    }
}
