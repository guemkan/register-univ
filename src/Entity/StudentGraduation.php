<?php

namespace App\Entity;

use App\Repository\StudentGraduationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentGraduationRepository::class)
 */
class StudentGraduation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameGrad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $speciality;

    /**
     * @ORM\Column(type="date")
     */
    private $delivrateDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationDelivrate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $schoolDelivrated;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $graduationFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameGrad(): ?string
    {
        return $this->nameGrad;
    }

    public function setNameGrad(?string $nameGrad): self
    {
        $this->nameGrad = $nameGrad;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getDelivrateDate(): ?\DateTimeInterface
    {
        return $this->delivrateDate;
    }

    public function setDelivrateDate(\DateTimeInterface $delivrateDate): self
    {
        $this->delivrateDate = $delivrateDate;

        return $this;
    }

    public function getLocationDelivrate(): ?string
    {
        return $this->locationDelivrate;
    }

    public function setLocationDelivrate(?string $locationDelivrate): self
    {
        $this->locationDelivrate = $locationDelivrate;

        return $this;
    }

    public function getSchoolDelivrated(): ?string
    {
        return $this->schoolDelivrated;
    }

    public function setSchoolDelivrated(?string $schoolDelivrated): self
    {
        $this->schoolDelivrated = $schoolDelivrated;

        return $this;
    }

    public function getGraduationFile(): ?string
    {
        return $this->graduationFile;
    }

    public function setGraduationFile(string $graduationFile): self
    {
        $this->graduationFile = $graduationFile;

        return $this;
    }
}
