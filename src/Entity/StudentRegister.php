<?php

namespace App\Entity;

use App\Repository\StudentRegisterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentRegisterRepository::class)
 */
class StudentRegister
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $num;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="studentRegisters")
     */
    private $person;

    /**
     * @ORM\OneToOne(targetEntity=Choice::class, cascade={"persist", "remove"})
     */
    private $choice;

    /**
     * @ORM\OneToOne(targetEntity=StudentGraduation::class, cascade={"persist", "remove"})
     */
    private $graduation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\OneToOne(targetEntity=StudentCv::class, cascade={"persist", "remove"})
     */
    private $parcours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNum(): ?string
    {
        return $this->num;
    }

    public function setNum(?string $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getChoice(): ?Choice
    {
        return $this->choice;
    }

    public function setChoice(?Choice $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    public function getGraduation(): ?StudentGraduation
    {
        return $this->graduation;
    }

    public function setGraduation(?StudentGraduation $graduation): self
    {
        $this->graduation = $graduation;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getParcours(): ?StudentCv
    {
        return $this->parcours;
    }

    public function setParcours(?StudentCv $parcours): self
    {
        $this->parcours = $parcours;

        return $this;
    }
}
