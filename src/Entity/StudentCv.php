<?php

namespace App\Entity;

use App\Repository\StudentCvRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentCvRepository::class)
 */
class StudentCv
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $cv = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCv(): ?array
    {
        return $this->cv;
    }

    public function setCv(array $cv): self
    {
        $this->cv = $cv;

        return $this;
    }
}
