<?php

namespace App\Entity;

use App\Repository\ChoiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ChoiceRepository::class)
 */
class Choice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"r"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=DepartmentChoice::class, cascade={"persist", "remove"})
     */
    private $first;

    /**
     * @ORM\OneToOne(targetEntity=DepartmentChoice::class, cascade={"persist", "remove"})
     */
    private $second;

    /**
     * @ORM\OneToOne(targetEntity=DepartmentChoice::class, cascade={"persist", "remove"})
     */
    private $last;

    /**
     * @ORM\ManyToOne(targetEntity=Cycle::class, inversedBy="choices")
     */
    private $cycleId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"r"})
     */
    private $level;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirst(): ?DepartmentChoice
    {
        return $this->first;
    }

    public function setFirst(?DepartmentChoice $first): self
    {
        $this->first = $first;

        return $this;
    }

    public function getSecond(): ?DepartmentChoice
    {
        return $this->second;
    }

    public function setSecond(?DepartmentChoice $second): self
    {
        $this->second = $second;

        return $this;
    }

    public function getLast(): ?DepartmentChoice
    {
        return $this->last;
    }

    public function setLast(?DepartmentChoice $last): self
    {
        $this->last = $last;

        return $this;
    }

    public function getCycleId(): ?Cycle
    {
        return $this->cycleId;
    }

    public function setCycleId(?Cycle $cycleId): self
    {
        $this->cycleId = $cycleId;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
