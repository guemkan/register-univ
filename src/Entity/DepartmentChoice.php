<?php

namespace App\Entity;

use App\Entity\Department;
use App\Entity\Filiere;
use App\Repository\DepartmentChoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepartmentChoiceRepository::class)
 */
class DepartmentChoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="departmentChoices")
     */
    private $departmentId;

    /**
     * @ORM\ManyToOne(targetEntity=Filiere::class, inversedBy="departmentChoices")
     */
    private $specialityId;

    /**
     * @ORM\Column(type="integer")
     */
    private $rang;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDepartmentId(): ?Department
    {
        return $this->departmentId;
    }

    public function setDepartmentId(?Department $departmentId): self
    {
        $this->departmentId = $departmentId;

        return $this;
    }

    public function getSpecialityId(): ?Filiere
    {
        return $this->specialityId;
    }

    public function setSpecialityId(?Filiere $specialityId): self
    {
        $this->specialityId = $specialityId;

        return $this;
    }

    public function getRang(): ?int
    {
        return $this->rang;
    }

    public function setRang(int $rang): self
    {
        $this->rang = $rang;

        return $this;
    }
}
