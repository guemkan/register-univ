<?php

namespace App\Repository;

use App\Entity\StudentRegister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StudentRegister>
 *
 * @method StudentRegister|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentRegister|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentRegister[]    findAll()
 * @method StudentRegister[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRegisterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentRegister::class);
    }

    public function add(StudentRegister $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(StudentRegister $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return StudentRegister[] Returns an array of StudentRegister objects
//     */
//    public function findByNum($value): array
//    {
//        return $this->createQueryBuilder('s')->select("s.person")
//            ->andWhere('s.num = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StudentRegister
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
