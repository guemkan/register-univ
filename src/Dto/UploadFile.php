<?php
namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
class UploadFile{

    /**
     * @Assert\NotBlank(message="Veuillez charger le diplome")
     * @Assert\File(mimeTypes={"application/pdf"})
     */
    private $image;
    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }
}