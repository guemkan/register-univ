<?php
namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ChoiceDTO{
    /**
     * @Assert\NotBlank
     */
    private $idCycle;
    /**
     * @Assert\NotBlank
     */
    private $idLevel;

    /**
     * @return mixed
     */
    public function getIdCycle():?int
    {
        return $this->idCycle;
    }

    /**
     * @param mixed $idCycle
     */
    public function setIdCycle(int $idCycle)
    {
        $this->idCycle = $idCycle;
    }

    /**
     * @return mixed
     */
    public function getIdLevel(): ?int
    {
        return $this->idLevel;
    }

    /**
     * @param mixed $idLevel
     */
    public function setIdLevel(int $idLevel)
    {
        $this->idLevel = $idLevel;
    }




}