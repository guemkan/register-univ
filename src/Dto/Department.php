<?php
namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
class Department{
    /**
     * @Assert\NotBlank
     */
    private $departement;
    private $speciality;
    private $rang;

    /**
     * @return mixed
     */
    public function getDepartement():?int
    {
        return $this->departement;
    }

    /**
     * @param mixed $departement
     */
    public function setDepartement(int $departement): void
    {
        $this->departement = $departement;
    }

    /**
     * @return mixed
     */
    public function getSpeciality():?int
    {
        return $this->speciality;
    }

    /**
     * @param mixed $speciality
     */
    public function setSpeciality(int $speciality): void
    {
        $this->speciality = $speciality;
    }

    /**
     * @return mixed
     */
    public function getRang():?int
    {
        return $this->rang;
    }

    /**
     * @param mixed $rang
     */
    public function setRang(int $rang): void
    {
        $this->rang = $rang;
    }



}