<?php
namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
class Gratuation{

    /**
     * @Assert\NotBlank
     */
    private $nameGrad;
    /**
     * @Assert\NotBlank
     */
    private $speciality;
    /**
     * @Assert\NotBlank
     */
    private $delivrateDate;
    /**
     * @Assert\NotBlank
     */
    private $locationDelivrate;
    /**
     * @Assert\NotBlank
     */
    private $schoolDelivrated;
    /**
     * @Assert\NotBlank
     */
    private $fileGraduation;

    /**
     * @return mixed
     */
    public function getNameGrad()
    {
        return $this->nameGrad;
    }

    /**
     * @param mixed $nameGrad
     */
    public function setNameGrad($nameGrad): void
    {
        $this->nameGrad = $nameGrad;
    }

    /**
     * @return mixed
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * @param mixed $speciality
     */
    public function setSpeciality($speciality): void
    {
        $this->speciality = $speciality;
    }

    /**
     * @return mixed
     */
    public function getDelivrateDate():?\DateTimeInterface
    {
        return $this->delivrateDate;
    }

    /**
     * @param mixed $delivrateDate
     */
    public function setDelivrateDate(\DateTimeInterface $delivrateDate): void
    {
        $this->delivrateDate = $delivrateDate;
    }

    /**
     * @return mixed
     */
    public function getLocationDelivrate():?string
    {
        return $this->locationDelivrate;
    }

    /**
     * @param mixed $locationDelivrate
     */
    public function setLocationDelivrate(string $locationDelivrate): void
    {
        $this->locationDelivrate = $locationDelivrate;
    }

    /**
     * @return mixed
     */
    public function getSchoolDelivrated():?string
    {
        return $this->schoolDelivrated;
    }

    /**
     * @param mixed $schoolDelivrated
     */
    public function setSchoolDelivrated(string $schoolDelivrated): void
    {
        $this->schoolDelivrated = $schoolDelivrated;
    }

    /**
     * @return mixed
     */
    public function getFileGraduation():?string
    {
        return $this->fileGraduation;
    }

    public function setFileGraduation(string $fileGraduation): void
    {
        $this->fileGraduation = $fileGraduation;
    }


}