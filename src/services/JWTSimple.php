<?php

namespace App\services;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class JWTSimple{
    private  $configuration;
    private $builder;

    public function __construct()
    {
        $this->configuration= Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText('%key%')
        );
        $this->builder = $this->configuration->builder();
    }
  public function addClaim($name,$value){
        $this->builder->withClaim($name,$value);
}
public function generatedToken(){
        $now = new \DateTimeImmutable();
        $token = $this->builder->issuedBy($_SERVER['HTTP_HOST'])
            ->permittedFor($_SERVER['HTTP_HOST'])
            ->identifiedBy(1)
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now)
            ->expiresAt($now->modify('+24 hour'))
            ->getToken($this->configuration->signer(),$this->configuration->signingKey());
        return $token->toString();
}
public function validateToken($jwt){
    try {
        $token = $this->configuration->parser()->parse($jwt);
        return true;
    }catch (\Exception $e){
        return false;
    }
//    $constraints = $this->configuration->validationConstraints();
//
//    if(!$this->configuration->validator()->validate($token, ...$constraints)){
//        return false;
////    }
//    return true;
}
public function getTokenClaims($jwt){
    $result = $this->validateToken($jwt);
    if ($result){
        $token = $this->configuration->parser()->parse($jwt);

        return $token->claims();
    }
    return null;
}

}
