<?php

namespace App\Controller;

use App\Dto\ChoiceDTO;
use App\Dto\Department;
use App\Dto\Gratuation;
use App\Dto\UploadFile;
use App\Entity\Choice;
use App\Entity\DepartmentChoice;
use App\Entity\Person;
use App\Entity\StudentCv;
use App\Entity\StudentGraduation;
use App\Entity\StudentRegister;
use App\Repository\ChoiceRepository;
use App\Repository\CycleRepository;
use App\Repository\DepartmentFiliereRepository;
use App\Repository\DepartmentRepository;
use App\Repository\FiliereRepository;
use App\Repository\LevelRepository;
use App\Repository\PersonRepository;
use App\Repository\RegionRepository;
use App\Repository\StudentCvRepository;
use App\Repository\StudentGraduationRepository;
use App\Repository\StudentRegisterRepository;
use App\services\FileUploadIn;
use App\services\JWTSimple;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Dompdf\Dompdf;
use Dompdf\Options;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="app_register",methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('register/index.html.twig', [
            'controller_name' => 'RegisterController',
        ]);
    }
//    /**
//     * @Route("/register/choice", name="app_register")
//     */
//    public function redToIndex(): Response
//    {
//        return $this->redirect('/register');
//    }

    /**
     * @Route("/register/person",name="reg_person",methods={"POST"})
     */
    public function savePerson(SerializerInterface $serializer,Request $request,ValidatorInterface $validator,EntityManagerInterface $em){
        try {
            $jsonR = $request->getContent();
            $post = $serializer->deserialize($jsonR,Person::class,'json');
            $error = $validator->validate($post);
            if (count($error)>0){
                return $this->json($error,400);
            }
            $em->persist($post);
            $em->flush();
            $jwt = new JWTSimple();
            $jwt->addClaim('id',$post->getId());
            return $this->json(['register'=>$jwt->generatedToken()],200,[]);

        }catch (NotEncodableValueException $e){
            return $this->json(
                [
                  'status'=>400,
                  'message'=>$e->getMessage()
                ],
                400
            );
        }
    }

    /**
     * @Route("/register/choice",name="reg_choice",methods={"POST"})
     */
    public function saveChoice(SerializerInterface $serializer,Request $request, ValidatorInterface $validator, EntityManagerInterface $em,DepartmentRepository $departmentRepository,FiliereRepository $filiereRepository,CycleRepository $cycleRepository){

        try {
            $jsonDecode = json_decode($request->getContent());
            $reg = $jsonDecode->register;

            $getData = $request->getContent();
            $choiceSeGet = $serializer->deserialize($getData,ChoiceDTO::class,'json');

            $errors= $validator->validate($choiceSeGet);
            if (count($errors)>0){
                return $this->json(
                    [
                        "messages"=>$errors
                    ],400
                );
            }
            $jwt = new JWTSimple();
           // $jwt->addClaim('id',$post->getId());
            if ($jwt->validateToken($reg)){
                $saved=false;
                $choice = json_decode($getData);
                $choiceToSave = new Choice();
                //$choiceDecode =
                if (isset($choice->choices) AND is_array($choice->choices) AND count($choice->choices)==3){
                    $i=0;
                    //dd($choice);
                    foreach ($choice->choices as $c) {
                        $getDepartment = new Department();

                        if (isset($c->department) AND !empty($c->department) AND is_int(intval(htmlspecialchars($c->department))))
                            $getDepartment->setDepartement(intval(htmlspecialchars($c->department)));
                        if (isset($c->speciality) AND !empty($c->speciality) AND is_int(intval(htmlspecialchars($c->speciality))))
                            $getDepartment->setSpeciality(intval(htmlspecialchars($c->speciality)));
                        if (isset($c->rang) AND !empty($c->rang) AND is_int(intval(htmlspecialchars($c->rang))))
                            $getDepartment->setRang(intval(htmlspecialchars($c->rang)));

                        $errorsD = $validator->validate($getDepartment);
                        if (count($errorsD)>0){
                            break;
                        }else{
                            $choiceSaveD = new DepartmentChoice();
                            //$dpR = $em->getRepository(Department::class);
                            $choiceSaveD->setDepartmentId($departmentRepository->find($getDepartment->getDepartement()));
                            if (!empty($getDepartment->getSpeciality())){
                                $choiceSaveD->setSpecialityId($filiereRepository->find($getDepartment->getSpeciality()));
                            }
                            $choiceSaveD->setRang($getDepartment->getRang());
                            $em->persist($choiceSaveD);
                            $em->flush();
                            if ($choiceSaveD->getRang()==1)
                                $choiceToSave->setFirst($choiceSaveD);
                            if ($choiceSaveD->getRang()==2)
                                $choiceToSave->setSecond($choiceSaveD);
                            if ($choiceSaveD->getRang()==3)
                                $choiceToSave->setLast($choiceSaveD);
                            //$idSaved[]=[$choiceSaveD->getRang()=>$choiceSaveD->getId()];
                        }

                        $i++;
                    }
                    $saved = true;
                }
                if ($saved==false){
                    return $this->json(
                        [
                            "messages"=>"not save"
                        ],400
                    );
                }

                $choiceToSave->setCycleId($cycleRepository->find($choiceSeGet->getIdCycle()));
                $choiceToSave->setLevel($choiceSeGet->getIdLevel());

                $em->persist($choiceToSave);
                $em->flush();
                $claims = $jwt->getTokenClaims($reg);
                $jwt->addClaim("id",$claims->get('id'));
                $jwt->addClaim('choice',$choiceToSave->getId());
                //$token =
                return $this->json(
                    [
                        "token"=>$jwt->generatedToken()
                    ],200
                );

            }else{
                return $this->json(
                    [
                        "messages"=>"Not valid"
                    ],400
                );
            }

        }catch (NotEncodableValueException $e){
            return $this->json(
                [
                    "messages"=>"Not valid"
                ],400
            );
        }

    }


    /**
     * @Route("/register/graduation/graduation",name="reg_diplome",methods={"POST"})
     */
    public function graduationFile(Request $request,ValidatorInterface $validator){
        $u = new UploadFile();
        $u->setImage($request->files->get('d'));
        $errors = $validator->validate($u);
        if (count($errors)>0){
            return $this->json(
                [
                    'message'=>$errors
                ],400
            );
        }
        $uploader = new FileUploadIn($this->getParameter("kernel.project_dir").'/public/diplomes/');
        return $this->json([
            "name"=>$uploader->upload($u->getImage())
        ]);
    }

    /**
     * @Route("/register/gratuation",name="reg_grad",methods={"POST"})
     */
    public function saveGratuation(Request $request,SerializerInterface $serializer,ValidatorInterface $validator,EntityManagerInterface $em)
    {
        try {
            $dataR = $request->getContent();
            $dataDes= $serializer->deserialize($dataR,Gratuation::class,'json');
            $errors = $validator->validate($dataDes);
            if (count($errors)>0){
                return $this->json(
                    [
                        "message"=>$errors
                    ],
                    400
                );
            }
            $jwt = new JWTSimple();
            // $jwt->addClaim('id',$post->getId());
            $jsonDecode = json_decode($request->getContent());
            if (isset($jsonDecode->register) AND !empty($jsonDecode->register)){

                if ($jwt->validateToken($jsonDecode->register)){
                    $reg = $jsonDecode->register;
                    $studentGratuation = new StudentGraduation();
                    $studentGratuation->setNameGrad($dataDes->getNameGrad());
                    $studentGratuation->setSpeciality($dataDes->getSpeciality());
                    $studentGratuation->setDelivrateDate($dataDes->getDelivrateDate());
                    $studentGratuation->setLocationDelivrate($dataDes->getLocationDelivrate());
                    $studentGratuation->setSchoolDelivrated($dataDes->getSchoolDelivrated());
                    $studentGratuation->setGraduationFile($dataDes->getFileGraduation());

                    $em->persist($studentGratuation);
                    $em->flush();
                    $jwt->addClaim('id',$jwt->getTokenClaims($reg)->get('id'));
                    $jwt->addClaim('choice',$jwt->getTokenClaims($reg)->get('choice'));
                    $jwt->addClaim('graduation',$studentGratuation->getId());

                    return $this->json(
                        [
                            "token"=>$jwt->generatedToken()
                        ],200
                    );
                }

            }
                return $this->json(
                    [
                        "messages"=>"Not valid"
                    ],400
                );


        }catch (NotEncodableValueException $e){
            return $this->json(
                [
                    "messages"=>"Not valid"
                ],400
            );
        }


    }

    private function getInterval(string $val ){
        switch (strlen($val)){
            case 1:
                return "0000".$val;
            case 2 :
                return "000".$val;
            case 3:
                return "00".$val;
        }
    }

    /**
     * @Route("/register/parcours",name="reg_parcours",methods={"POST"})
     */
    public function saveCv(Request $request,StudentCvRepository $studentCvRepository,SerializerInterface $serializer,ValidatorInterface $validator,StudentRegisterRepository $registerRepository,EntityManagerInterface $em,PersonRepository $personRepository,ChoiceRepository $choiceRepository,StudentGraduationRepository $graduationRepository){
        try {
            $data = json_decode($request->getContent());
            $jwt = new JWTSimple();
            if (isset($data->register) AND !empty($data->register)){
                if ($jwt->validateToken($data->register)){
                    $register = $data->register;
                    $array = [];
                    $array["year"]=$data->year;
                    $array["school"]=$data->school;
                    $array["level"]=$data->level;
                    $array["graduation"]=$data->graduation;
                    $cv = new StudentCv();
                    $cv->setCv($array);
                    $em->persist($cv);
                    $em->flush();
                    $person= $personRepository->find($jwt->getTokenClaims($register)->get('id'));
//                    $query= $em->createQuery(
//                        '
//                        SELECT c
//                        FROM App:Choice c
//                        WHERE c.id=:d
//                        '
//                    )->setParameter('d',$jwt->getTokenClaims($register)->get('choice'));
                    $choice = $choiceRepository->find($jwt->getTokenClaims($register)->get('choice'));
                    $grad=$graduationRepository->find($jwt->getTokenClaims($register)->get('graduation'));//date('Y',strtotime('+1 year'))





                    $now= new \DateTimeImmutable();
                    $reg = new StudentRegister();
                    $reg->setChoice($choice);
                    $reg->setGraduation($grad);
                    $reg->setPerson($person);
                    $reg->setParcours($cv);
                    $reg->setRegisterDate($now);
                    $date = new \DateTime();
                    $y = str_split(date('Y'),2);

                    //$reg->setNum($y[count($y)-1].uniqid("PI").$this->getInterval(count($registerRepository->findAll())+1));
                    $reg->setNum($y[count($y)-1].strtoupper(uniqid("PI")));

                    $em->persist($reg);
                    $em->flush();

                    $jwt->addClaim('id',$person->getId());
                    $jwt->addClaim('choice',$choice->getId());
                    $jwt->addClaim('graduation',$grad->getId());
                    $jwt->addClaim('register',$reg->getId());

                    return $this->json(
                        [
                            "token"=>$jwt->generatedToken(),
                            "num"=>$reg->getNum(),
                            "date"=>$reg->getRegisterDate()->format("d/m/Y")
                        ],200
                    );
                }

            }
            return $this->json(
                [
                    "messages"=>"Not valid"
                ],400
            );

        }catch (\Exception $e){
            return $this->json(
                [
                    "messages"=>$e->getMessage()
                ],400
            );
        }
    }


    /**
     * @Route("/register/fiche/{num}",name="p")
     */
    public function getPerson(string $num,StudentRegisterRepository $studentRegisterRepository,EntityManagerInterface $em,PersonRepository $personRepository){

        try {
            $getData = $studentRegisterRepository->findBy(["num"=>$num]);
            $maxNumber = count($getData)-1;
            $pdfOpt = new Options();
            $pdfOpt->set('defaultFont','Arial');

            $dompdf = new Dompdf($pdfOpt);
            $html=$this->renderView('prints/print.html.twig',[
                "name"=>$getData[$maxNumber]->getPerson()->getFirstName(),
                "lastName"=>$getData[$maxNumber]->getPerson()->getLastName(),
                "num"=>$getData[$maxNumber]->getNum(),
                "date"=>$getData[$maxNumber]->getRegisterDate()->format('d/m/Y'),
                "first"=>$getData[$maxNumber]->getChoice()->getFirst()->getDepartmentId()->getName(),
                "second"=>$getData[$maxNumber]->getChoice()->getSecond()->getDepartmentId()->getName(),
                "last"=>$getData[$maxNumber]->getChoice()->getLast()->getDepartmentId()->getName(),
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A5','portrait');
            $dompdf->render();
            $dompdf->stream($getData[$maxNumber]->getPerson()->getFirstName()."-".uniqid().".pdf");
            return $this->json([
                "ok"=>"ok"
            ]);
        }catch (\Exception $e){
            return $this->json(
                [
                    'message'=>$e->getMessage()
                ]
            );
        }

    }

    /**
     * @Route("/register/region",name="reg_get_region",methods={"GET"})
     */
    public function getRegion(RegionRepository $regionRepository){
        return $this->json($regionRepository->findAll(),200,[]);
    }

    /**
     * @Route("/register/level",name="reg_get_level",methods={"GET"})
     */
    public function getLevel(LevelRepository $levelRepository){
        return $this->json($levelRepository->findAll(),200,[],['groups'=>'r']);
    }
    /**
     * @Route("/register/department",name="reg_get_dep",methods={"GET"})
     */
    public function getDepartment(DepartmentRepository $departmentRepository,JWTTokenManagerInterface $JWTTokenManager){
//        $jwt = new JWTSimple();
//        $jwt->addClaim('test',"1234");
//        $jwt->addClaim('name',"Dietrich");
//        return $this->json(['token'=> $jwt->generatedToken()]);
        return $this->json($departmentRepository->findAll(),200,[],['groups'=>'r']);
    }
    /**
     * @Route("/register/speciality",name="reg_get_spe",methods={"GET"})
     */
    public function getSpeciality(FiliereRepository $filiereRepository){
        return $this->json($filiereRepository->findAll(),200,[],['groups'=>'r']);
    }
    /**
     * @Route("/register/cycle",name="reg_get_cycle",methods={"GET"})
     */
    public function getCycle(CycleRepository $cycleRepository){
        return $this->json($cycleRepository->findAll(),200,[],['groups'=>'r']);
    }

    /**
     * @Route("/register/df",name="reg_get_df",methods={"GET"})
     */
    public function getDepartmentFiliere(DepartmentFiliereRepository $departmentFiliereRepository,DepartmentRepository $departmentRepository){
        $allData = $departmentFiliereRepository->findAll();
        $sendData = [];
        $dep = $departmentRepository->findAll();
        foreach ($dep as $d){
            $filieres = [];
            foreach ($allData as $df){
                if ($d->getId() == $df->getDepartment()->getId()){
                    $filieres[]=$df->getFiliere();
                }

        }
            $sendData[]= [
                "departement"=>$d->getId(),
                "filiere"=>$filieres,
            ];
        }

        return $this->json($sendData,200,[],['groups'=>'r']);

    }
}
