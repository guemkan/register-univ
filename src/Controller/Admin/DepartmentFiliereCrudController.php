<?php

namespace App\Controller\Admin;

use App\Entity\DepartmentFiliere;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DepartmentFiliereCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DepartmentFiliere::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('department'),
            AssociationField::new('filiere'),
            BooleanField::new('active'),
        ];
    }

}
