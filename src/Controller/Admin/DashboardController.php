<?php

namespace App\Controller\Admin;

use App\Entity\Cycle;
use App\Entity\Department;
use App\Entity\DepartmentFiliere;
use App\Entity\Filiere;
use App\Entity\Region;
use App\Repository\SchoolRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    private $_school;
    public function __construct(SchoolRepository $schoolRepository)
    {
        $this->_school = $schoolRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        $nameSchool = null;
        $getInformationSchool = $this->_school->findAll();
        foreach ($getInformationSchool as $s){
            $nameSchool = $s->getSchoolName();
        }
       // $this->
        return Dashboard::new()
            ->setTitle((empty($nameSchool))?'Configurer votre faculté':$nameSchool);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Home', 'fas fa-home');
        yield MenuItem::section('Configuration','fa fa-gear');
            yield MenuItem::subMenu('Régions','')->setSubItems(
                [
                    MenuItem::linkToCrud('Ajouter','fas fa-plus',Region::class)->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('Liste','fas fa-eye',Region::class),
                ]
            );
            yield MenuItem::subMenu('Cycles','')->setSubItems(
                [
                    MenuItem::linkToCrud('Ajouter','fas fa-plus',Cycle::class)->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('Liste','fas fa-eye',Cycle::class),
                ]
            );
            yield MenuItem::subMenu('Départements','')->setSubItems(
                [
                    MenuItem::linkToCrud('Ajouter','fas fa-plus',Department::class)->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('Liste','fas fa-eye',Department::class),
                ]
            );
            yield MenuItem::subMenu('Filières','')->setSubItems(
                [
                    MenuItem::linkToCrud('Ajouter','fas fa-plus',Filiere::class)->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('Liste','fas fa-eye',Filiere::class),
                ]
            );
            yield MenuItem::linkToCrud('filiere par département','',DepartmentFiliere::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
