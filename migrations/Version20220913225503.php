<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220913225503 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE student_cv (id INT AUTO_INCREMENT NOT NULL, cv JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student_register (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, choice_id INT DEFAULT NULL, graduation_id INT DEFAULT NULL, num VARCHAR(255) DEFAULT NULL, register_date DATETIME NOT NULL, INDEX IDX_EC720C8F217BBB47 (person_id), UNIQUE INDEX UNIQ_EC720C8F998666D1 (choice_id), UNIQUE INDEX UNIQ_EC720C8F35E19886 (graduation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE student_register ADD CONSTRAINT FK_EC720C8F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE student_register ADD CONSTRAINT FK_EC720C8F998666D1 FOREIGN KEY (choice_id) REFERENCES choice (id)');
        $this->addSql('ALTER TABLE student_register ADD CONSTRAINT FK_EC720C8F35E19886 FOREIGN KEY (graduation_id) REFERENCES student_graduation (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE student_cv');
        $this->addSql('DROP TABLE student_register');
    }
}
