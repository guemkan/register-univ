<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220913201215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE choice (id INT AUTO_INCREMENT NOT NULL, first_id INT DEFAULT NULL, second_id INT DEFAULT NULL, last_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_C1AB5A92E84D625F (first_id), UNIQUE INDEX UNIQ_C1AB5A92FF961BCC (second_id), UNIQUE INDEX UNIQ_C1AB5A92D1FD94E6 (last_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE choice ADD CONSTRAINT FK_C1AB5A92E84D625F FOREIGN KEY (first_id) REFERENCES department_choice (id)');
        $this->addSql('ALTER TABLE choice ADD CONSTRAINT FK_C1AB5A92FF961BCC FOREIGN KEY (second_id) REFERENCES department_choice (id)');
        $this->addSql('ALTER TABLE choice ADD CONSTRAINT FK_C1AB5A92D1FD94E6 FOREIGN KEY (last_id) REFERENCES department_choice (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE choice');
    }
}
