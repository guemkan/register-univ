<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220912064029 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE department_choice ADD speciality_id_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP department_id, CHANGE speciality_id department_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE department_choice ADD CONSTRAINT FK_D5B1A56264E7214B FOREIGN KEY (department_id_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE department_choice ADD CONSTRAINT FK_D5B1A562ADE0D45C FOREIGN KEY (speciality_id_id) REFERENCES filiere (id)');
        $this->addSql('CREATE INDEX IDX_D5B1A56264E7214B ON department_choice (department_id_id)');
        $this->addSql('CREATE INDEX IDX_D5B1A562ADE0D45C ON department_choice (speciality_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE department_choice DROP FOREIGN KEY FK_D5B1A56264E7214B');
        $this->addSql('ALTER TABLE department_choice DROP FOREIGN KEY FK_D5B1A562ADE0D45C');
        $this->addSql('DROP INDEX IDX_D5B1A56264E7214B ON department_choice');
        $this->addSql('DROP INDEX IDX_D5B1A562ADE0D45C ON department_choice');
        $this->addSql('ALTER TABLE department_choice ADD department_id INT NOT NULL, ADD speciality_id INT DEFAULT NULL, DROP department_id_id, DROP speciality_id_id, DROP created_at');
    }
}
