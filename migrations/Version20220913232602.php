<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220913232602 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student_register ADD parcours_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student_register ADD CONSTRAINT FK_EC720C8F6E38C0DB FOREIGN KEY (parcours_id) REFERENCES student_cv (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EC720C8F6E38C0DB ON student_register (parcours_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student_register DROP FOREIGN KEY FK_EC720C8F6E38C0DB');
        $this->addSql('DROP INDEX UNIQ_EC720C8F6E38C0DB ON student_register');
        $this->addSql('ALTER TABLE student_register DROP parcours_id');
    }
}
